'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

gulp.task('sass', ()=>{
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
})

gulp.task('sass:watch', ()=>{
    gulp.watch('./css/*.scss',['sass']);

});

/*  gulp.task('browser-sync' , ()=>{
    var files = ['./*.html', './css/*.css', './images/*.{png, jpg, gif}', './js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
        
    });
});*/

gulp.task('default',  ()=>{
    gulp.watch('sass:watch');
    var files = ['./*.html', './css/*.css', './images/*.{png, jpg, gif}', './js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
        
    });
    
});

gulp.task('clean', ()=>{
    return del(['dist']);
});

gulp.task('copyfonts', ()=>{
    gulp.src('.node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('./dist/fonts'));
})

//ojo
gulp.task('imagemin', ()=>{
    return gulp.src('./images/*.{png, jpg, jpeg, gif }')
    .pipe(imagemin({
        optimizationLevel: 3,
        interlaced: true,
        progressive: true
    }))
    .pipe(gulp.dest('dist/images'))
});

gulp.task('usemin', ()=>{
    return gulp.src('./*.html')
    .pipe(flatmap((stream, file)=>{
        return stream
            .pipe(usemin({
                css: [rev()],
                html: [()=>{return htmlmin({collapseWhitespace: true})}],
                js: [uglify(),rev()],
                inlinejs:[uglify()],
                inlinecss:[cleanCss(), 'concat']
                
            }));     
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('build', ()=>{
    //console.log('hola mundo');
    //gulp.series('copyfonts', 'imagemin', 'usemin');
    //gulp.src('.node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    //.pipe(gulp.dest('./dist/fonts'));

        return gulp.src('./images/*.jpg')
        .pipe(imagemin({
            optimizationLevel: 3,
            interlaced: true,
            progressive: true
        }))
        .pipe(gulp.dest('dist/images'))

    /*return gulp.src('./*.html')
    .pipe(flatmap((stream, file)=>{
        return stream
            .pipe(usemin({
                css: [rev()],
                html: [()=>{return htmlmin({collapseWhitespace: true})}],
                js: [uglify(),rev()],
                inlinejs:[uglify()],
                inlinecss:[cleanCss(), 'concat']
                
            }));     
    }))
    .pipe(gulp.dest('dist/'));*/
});
